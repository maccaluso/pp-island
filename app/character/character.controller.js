(function () {
    'use strict';

    angular
        .module('app.character')
        .controller('CharacterController', CharacterController);

    CharacterController.$inject = ['$state', '$stateParams', '$q', 'dataservice', '$window', '$interval'];
    /* @ngInject */
    function CharacterController($state, $stateParams, $q, dataservice, $window, $interval) {
        var vm = this;

        vm.characterData = null;
        vm.answers = [
            {
                iddomanda: null,
                idrisposta: null
            },
            {
                iddomanda: null,
                idrisposta: null
            },
            {
                iddomanda: null,
                idrisposta: null
            },
            {
                iddomanda: null,
                idrisposta: null
            }
        ];
        vm.errore = false;
        vm.charID = $stateParams.id;
        vm.showModal = false;
        vm.canCloseModal = false;
        vm.results = [];
        vm.showQuestions = false;
        vm.percentElapsed = 100;

        vm.validateAnswers = validateAnswers;

        activate();

        function activate() {
            var promises = [dataservice.getInitialState(), dataservice.getCharacterById( vm.charID )];
            // var promises = [dataservice.getCharacterById( vm.charID )];
            return $q.all(promises).then(function(data) {
                if(data[0].errore)
                {
                    vm.errore = true;
                    vm.messageTitle = 'Errore';
                    vm.messageType = 'error';

                    vm.message = 'Qualcosa non ha funzionato! Prova ricaricare la pagina.';

                    vm.showModal = true;
                }
                else
                {
                    vm.characterData = data[1];
                    for(var i=0; i<data[1].questionario.length; i++)
                    {
                        vm.answers[i].iddomanda = data[1].questionario[i].iddomanda;
                    }
                    
                    angular.element(document.querySelector('#questions'))[0].style.height = $window.innerHeight - 130 + 'px';
                    angular.element(document.querySelector('#questionsForm'))[0].style.height = $window.innerHeight - 130 + 'px';
                    angular.element( document.querySelector('#loadingScreen') ).addClass('fadeOut')
                }

                var readingDelay = 15000;
                var startTime = Date.now();

                var elapse = $interval(function() {
                    var elapsed = Date.now() - startTime
                    vm.percentElapsed = Math.round(elapsed/(readingDelay / 100));
                    if( (Date.now() - startTime) >= readingDelay ) { 
                        vm.showQuestions = true;
                        $interval.cancel(elapse); 
                    }
                }, 1)
            });
        }

        function validateAnswers() {
            var errors = 0;
            for(var i=0; i<vm.answers.length; i++)
            {
                if(vm.answers[i].idrisposta == null) {
                    errors++;
                }
            }
            if(errors > 0)
            {
                vm.messageTitle = 'Ops... Attenzione';
                vm.messageType = 'warning';
                vm.message = 'Controlla di aver risposto a tutte e 4 le domande prima di procedere.';

                vm.canCloseModal = true;
                vm.showModal = true;
            }
            else
            {
                sendAnswers();
            }
        }

        function sendAnswers() {
            var promises = [dataservice.sendAnswers( $stateParams.id, vm.answers )];
            return $q.all(promises).then(function(data) {
                if(data[0].giuste < 3) {
                    vm.messageTitle = 'Peccato!';
                    vm.messageType = 'error';
                    vm.message = 'Devi ripetere la prova o tornare sull\'isola';
                    vm.canCloseModal = true;
                }
                if(data[0].giuste == 3) {
                    vm.messageTitle = 'Ben fatto!';
                    vm.messageType = 'success';
                    vm.message = ' Ma puoi migliorare. Hai risposto correttamente a 3 domande su 4.';
                    vm.canCloseModal = true;
                }
                if(data[0].giuste == 4) {
                    vm.messageTitle = 'Perfetto!';
                    vm.messageType = 'success';
                    vm.message = 'Hai risposto correttamente a tutte e 4 le domande.';
                    vm.canCloseModal = false;
                    vm.ctaBlock = true;
                }

                if(data[0].errore)
                {
                    console.log(data[0]);
                }
                else
                {
                    for(var i=0; i<data[0].risultato.length; i++){
                        // if( !data[0].risultato[i].corretto ) {
                            // vm.answers[i].iddomanda = null;
                            vm.answers[i].idrisposta = null;
                        // }
                    }
                    vm.results = data[0].risultato;

                    vm.showModal = true;
                }
            });
        }
    }
})();
