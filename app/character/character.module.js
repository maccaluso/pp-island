(function () {
    'use strict';

    angular.module('app.character', [
        'app.core'
    ]);

})();