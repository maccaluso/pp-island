(function() {
    'use strict';

    angular
        .module('app.character')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'character',
                config: {
                    url: '/character/:id',
                    templateUrl: 'app/character/character.html',
                    controller: 'CharacterController',
                    controllerAs: 'vm',
                    title: 'Scheda personaggio',
                    otherwise: '/',
                    data: {
                        requireLogin: false
                    }
                }
            }
        ];
    }
})();
