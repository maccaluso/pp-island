/* global toastr:false, moment:false */
(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('ENV', {
        	mode: 'dev',
        	mock: '/data/mock.json',
        	local: true
        });
})();
