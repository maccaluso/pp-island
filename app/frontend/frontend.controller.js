(function () {
    'use strict';

    angular
        .module('app.frontend')
        .controller('FrontendController', FrontendController);

    FrontendController.$inject = ['$window', '$state', '$q', '$scope', 'dataservice', 'localStorageService', '$timeout'];
    /* @ngInject */
    function FrontendController($window, $state, $q, $scope, dataservice, localStorageService, $timeout) {
        var vm = this;
        vm.caseDescription = null;
        vm.initialState = null;
        vm.goClass = 'wait';
        vm.showInfoModal = false;
        vm.accept = false;
        vm.tso = false;
        vm.canCompleteTrial = false;

        vm.charIsActive = charIsActive;
        vm.fakeSave = fakeSave;
        vm.showInfo = showInfo;
        vm.validateAccept = validateAccept;
        vm.finalize = finalize;

        activate();

        function activate() {
            var promises = [dataservice.getInitialState()];
            return $q.all(promises).then(function(data) {

                parseInitialState(data[0]);

                angular.element( document.querySelector('#loadingScreen') ).addClass('fadeOut');

                var w = angular.element($window);
                var bp = document.querySelector( '#bluePrint' );
                var tl = new TimelineMax({delay:0.5});

                var adjustBluePrintBoundingBox = function() {
                    var islandWidth = ( $window.innerWidth * 80 ) / 100;
                    var islandHeight = (islandWidth * 405)/930;

                    var islandHeightPercent = (islandHeight*100) / $window.innerHeight;
                    
                    angular.element(bp).css({
                        height: islandHeightPercent + '%',
                        top: (100 - islandHeightPercent) / 2 + '%'
                    });
                }
                
                var adjustCharPositions = function() {
                    for(var i=0; i<data[0].length; i++)
                    {
                        var el = document.querySelector( '#char' + data[0][i].pallino );
                        var elSize;
                        if(el) { elSize = el.clientWidth; }
                        var tso = document.querySelector( '#tso' + data[0][i].pallino );
                        var dest = document.querySelector( '#dest' + data[0][i].pallino );
                        var timespan = 0.5 * (i/5);

                        var giuste = data[0][i].giuste;
                        if(giuste > 2)
                        {
                            if(giuste == 3) {
                                if(data[0][i].risultato[0] && data[0][i].risultato[1])
                                {
                                    // ha sbagliato la cura
                                    tl.invalidate()
                                      .to(el, timespan, {top:(tso.getBoundingClientRect().top - (elSize/2)) + 'px'}, "tso")
                                      .to(el, timespan, {left:(tso.getBoundingClientRect().left - (elSize/2)) + 'px'}, "tso")
                                }
                                // if(data[0][i].risultato[2] && data[0][i].risultato[3])
                                // {
                                //     // ha sbagliato la profilazione
                                // }
                            }
                            if(giuste == 4) {
                                tl.invalidate()
                                  .to(el, timespan, {top:(dest.getBoundingClientRect().top - (elSize/2)) + 'px'}, "giuste")
                                  .to(el, timespan, {left:(dest.getBoundingClientRect().left - (elSize/2)) + 'px'}, "giuste")
                            }
                        }
                    }
                }

                angular.element(document).ready(function () {
                    adjustBluePrintBoundingBox();
                    adjustCharPositions();
                });

                w.bind('resize', function () {
                    adjustBluePrintBoundingBox();
                    adjustCharPositions();
                });

                if( localStorageService.get('accept') )
                {
                    vm.showInfoModal = false;
                    vm.accept = true;
                }
                else
                {
                    vm.showInfoModal = true;
                    vm.accept = false;
                }
            });
        }

        function parseInitialState(initialState){

            var count = 0;
            for(var i=0; i<initialState.length; i++)
            {
                var giuste = initialState[i].giuste;
                if(giuste > 2)
                {
                    if(giuste == 3) {
                        if(initialState[i].risultato[0] && initialState[i].risultato[1])
                        {
                            // ha sbagliato la cura
                            initialState[i].state = 'tso';
                            initialState[i].col = 'orange';
                        }
                        if(initialState[i].risultato[2] && initialState[i].risultato[3])
                        {
                            // ha sbagliato la profilazione
                            initialState[i].state = 'sea';
                            initialState[i].col = 'orange';
                        }
                    }
                    if(giuste == 4) {
                        // tutto giusto
                        initialState[i].state = 'done';
                        initialState[i].col = 'green';
                    }
                    count++;
                }
            }

            if(count >= initialState.length) { vm.canCompleteTrial = true; }
            vm.initialState = initialState;
        }

        function charIsActive(charID,correctAnswers) {
            if(correctAnswers == 4)
            {
                vm.charAvatarPath = 'dist/img/avatar/' + charID;
                vm.characterProfile = vm.initialState[charID-1].descrizione;
                vm.alertCharIsBlocked = true;
            }
            else
            {
                $state.go( 'character', { id: charID } );
            }
        }

        function fakeSave() {
            vm.saveTitle = 'Attendi...';
            vm.showSaveModal = true;
            $timeout(function() {
                vm.saveTitle = 'Prova salvata';
                $timeout(function() {
                    vm.showSaveModal = false;
                }, 500);
            }, 1200);
        }

        function showInfo() { vm.showInfoModal = true; }

        function validateAccept() {
            if( !vm.acceptCheck )
            {
                console.log('devi checkare');
            }
            else
            {
                userAccepted();
            }
        }
        function userAccepted() {
            localStorageService.set('accept',true);
            vm.accept = true;
            vm.showInfoModal = false;
        }

        function finalize() {
            if(vm.canCompleteTrial)
            {
                var promises = [dataservice.finalize()];
                return $q.all(promises).then(function(data) {
                    $window.location.href = 'http://ecm.project-communication.it/enter.asp?idevento=150';
                });
            }
        }
    }
})();
