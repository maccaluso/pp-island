(function () {
    'use strict';

    angular.module('app.frontend', [
        'app.core',
        'ngAnimate',
        'LocalStorageModule'
    ]);

})();