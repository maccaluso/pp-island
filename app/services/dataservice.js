(function () {
    'use strict';

    var positions = [
        {
            start: { x:0, y:0 },
            end: { x:0, y:200 },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:61, y:-15 },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
        {
            start: { x:'32%', y:'14%' },
            end: { x:'25%', y:'61%' },
        },
    ]
    
    angular
        .module('app.core')
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$http', '$q', '$filter', 'ENV'];
    /* @ngInject */
    function dataservice($http, $q, $filter, ENV) {
        var session;
        var baseEndPoint = 'http://ecm.project-communication.it/ppisland/';
        var service = {
            getInitialState: getInitialState,
            getCharacterById: getCharacterById,
            sendAnswers: sendAnswers,
            finalize: finalize,
            getSession: getSession
        };
        return service;

        function getInitialState() {

            var startEndpoint;
            ENV.mode == 'dev' ? startEndpoint = ENV.mock : startEndpoint = baseEndPoint + 'start.asp';
            return $http.get(startEndpoint)
                        .then(getInitialStateComplete)
                        .catch(getInitialStateFailed);

            function getInitialStateComplete(response) {
                session = response.data.sessione;
                for(var i=0; i<response.data.situazione.length; i++)
                {
                    response.data.situazione[i].position = positions[i];
                }
                return response.data.situazione;
            }
            function getInitialStateFailed(error) {
                console.log('XHR Failed for getQuestions.' + error.data);
            }
        }

        function getCharacterById(charID) {
            var getCharByIDEndpoint;
            ENV.mode == 'dev' ? getCharByIDEndpoint = '/data/char' + charID + '.json' : getCharByIDEndpoint = baseEndPoint + 'json.asp?pallino=' + charID;
            return $http.get(getCharByIDEndpoint)
                        .then(getCharByIDComplete)
                        .catch(getCharByIDFailed);

            function getCharByIDComplete(response) {
                return response.data;
            }
            function getCharByIDFailed(error) {
                console.log('XHR Failed for getCharByID.' + error.data);
            }
        }

        function sendAnswers(id,answers) {

            var data = {
                sessione: session,
                pallino: id,
                risposte: answers
            }
            
            var postAnswersEndpoint = baseEndPoint + 'jsonrisposte.asp';

            return $http.post( postAnswersEndpoint, data )
                        .then(postAnswersComplete)
                        .catch(postAnswersFailed);

            function postAnswersComplete(response) {
                return response.data;
            }
            function postAnswersFailed(error) {
                console.log('XHR Failed for getCharByID.' + error.data, error);
            }
        }

        function finalize() {
            var finalizeEndpoint;
            ENV.mode == 'dev' ? finalizeEndpoint = 'data/endresponse.json' : finalizeEndpoint = baseEndPoint + 'finalizza.asp';
            return $http.get(finalizeEndpoint)
                        .then(finalizeComplete)
                        .catch(finalizeFailed);

            function finalizeComplete(response) {
                // session = response.data.sessione;
                return response;
            }
            function finalizeFailed(error) {
                console.log('XHR Failed for finalize.' + error.data);
            }
        }

        ////////////////////////
        /* - SHARED HELPERS - */
        ////////////////////////

        function getSession() { return session; }
    }
})();
