(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('localstorage', localstorage);

    localstorage.$inject = ['$window', '$rootScope'];
    /* @ngInject */
    function localstorage($window, $rootScope) {
        var service = {

            setData: setData,
            getData: getData

        };

        return service;

        function setData(key, val) {
            // $window.localStorage && $window.localStorage.setItem(key, val);
            // return this;
        }

        function getData(key) {
            // return $window.localStorage && $window.localStorage.getItem(key);
        }

        // function getDeals() {
        //     // return $q.when(deals);
        // }
        // function getDealById(dealId) {
        //     var deal = $filter('filter')(deals, function (d) {
        //         return d.id == dealId;
        //     })[0];
        //     return $q.when( deal );
        // }

        // function getLocationCount() { return $q.when(2); }
        // function getLocations() {
        //     return $q.when(locations);
        // }
        // function getLocationById(locationId) {
        //     var location = $filter('filter')(locations, function (d) {
        //         return d.id == locationId;
        //     })[0];
        //     return $q.when( location );
        // }

        // function getMessageCount() { return $q.when(72); }

        // function getPeople() {
        //     return $http.get('/api/people')
        //         .then(success)
        //         .catch(fail);

        //     function success(response) {
        //         return response.data;
        //     }

        //     function fail(error) {
        //         var msg = 'query for people failed. ' + error.data.description;
        //         logger.error(msg);
        //         return $q.reject(msg);
        //     }
        // }
    }
})();
